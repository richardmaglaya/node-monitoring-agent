/**
 * Created by richardmaglaya on 2016-11-25.
 */
'use strict';

var http = require('http');
var Slack = require('slack-node');
var apiResponse = null;
var webhookUri = 'https://hooks.slack.com/services/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';
var serviceName, port, greenNodeAgent, redNodeAgent;
console.log('Node-Monitoring Service is live');

var isValueWithinRange = function (value, range) {
	if (range.floor && range.ceiling) {
		if (value > range.floor && value < range.ceiling) {
			return true;
		}
	} else if (range.floor && !(range.ceiling)) {
		if (value >= range.floor) {
			return true;
		}
	} else if (!(range.floor) && range.ceiling) {
		if (value <= range.ceiling) {
			return true;
		}
	}
};


//-- Private method
//-- Either 'resu.me' or 'internal-api.resume.com'
var checkService = function() {
	
	if (process.env.NODE_AGENT === 'internal-api') {
		serviceName = 'resume-service-internal';
		greenNodeAgent = 'internal-api-green-node-agent';
		redNodeAgent = 'internal-api-red-node-agent';
	} else if (process.env.NODE_AGENT === 'shortlink'){
		serviceName = 'resu.me';
		greenNodeAgent = 'shortlink-green-node-agent';
		redNodeAgent = 'shortlink-red-node-agent';
	};
	port = require('./../' + serviceName + '/config/server').config.express.shared.server_port;
	var options = {
		host: 'localhost',
		port: port,
		path: '/health_check'
	};
	//-- console.log('%j\n\n', options);
	http.get(options, function(res) {
		//-- console.log('Got ' + options.host + ' response: ' + res.statusCode);
		//-- console.log('HEADERS: ' + JSON.stringify(res.headers));
		res.setEncoding('utf8');
		res.on('data', function (chunk) {
			apiResponse = chunk;
			//-- console.log('BODY: '+ chunk);
		});
		res.on('end', function() {
			//-- console.log('No more data in response.');
			postToSlack(greenNodeAgent, apiResponse, ':ok_hand:');
		});
	}).on('error', function(e) {
		//-- console.log('Got error: ' + e.message);
		postToSlack(redNodeAgent, e.message, ':rage:');
	});
};


//-- Private method
var postToSlack = function(username, text, iconEmoji) {
	
	var slack = new Slack();
	slack.setWebhook(webhookUri);
	slack.webhook({
		channel: '#alerts',
		username: username,
		text: text,
		icon_emoji: iconEmoji
	}, function(err, response) {
		console.log(response);
	});
};


setInterval(function(){
	console.log('Checking service ...');
	checkService();
}, 6000);


process.on(['SIGINT', 'SIGTERM'], function() {
	console.log('Node-Monitoring Agent is shutting down');
	process.exit();
});