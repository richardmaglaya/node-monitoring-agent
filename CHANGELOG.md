# Resume-Service-Internal ChangeLog


### 2016.11.25 Version 0.1.0 (Unstable)
***

* [RDPCA-83](https://resumecom.atlassian.net/browse/RDPCA-83): Implement Alerts system integrating @Slack for Node apps


##################################################
# PLEASE add the new JIRAs to the TOP of this log
##################################################