# When running the *resume-services-internal*

1. Clone `resume-service-internal` to your local machine. Git repository is at https://github.com/ResumeServices/resume-service-internal.git
2. Run the app from command line: `$ node app`


# To verify the *resume-services-internal* 

3. Pull the API information to your browser: http://localhost:9000/api/systemInfo/
4. Pull one (1) User Account Information to your browser: http://localhost:9000/v1/users/:user_id

